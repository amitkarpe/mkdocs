hero: Metadata enables hero teaser texts
path: blob/master
source: docs/snippets.md

[TOC]

# Objective
# Install and setup the service

#Test Net 1 -  https://github.com/commitHub/genesisTransactions/tree/master/crust-1
#Test Net 2 - https://github.com/commitHub/genesisTransactions/blob/master/crust-2/docs/1.setup.md

# Add user as "root"

project=commit;
git="https://github.com/commitHub/commitBlockchain"
user=commit;
version=version0.1
genesis="https://github.com/commitHub/genesisTransactions"
sudo useradd ${user}  -d /opt/validation/${user} -s /bin/bash -m
sudo passwd ${user}
sudo su - ${user}

# Update system software as "root"

```bash
# Updates ubuntu
sudo apt update
sudo apt upgrade -y
# Installs packages necessary to run go
sudo apt install build-essential -y

# Installs go
wget https://dl.google.com/go/go1.12.5.linux-amd64.tar.gz
sudo tar -xvf go1.12.5.linux-amd64.tar.gz
sudo mv go /usr/local
```


# Updates environmental variables to include go
```bash
cat <<EOF>> ~/.profile
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
EOF
source ~/.profile
```

# Verify that go is installed run 
```bash
go version
``` 

# download project from GiHub

```bash
echo $project
echo $git
git clone $git
cd commitBlockchain
git status
git branch -a
git checkout ${version}
make install
```

# Verfiy new binaries 

> ```
    $ echo $HOME/go/bin
    /opt/validation/commit/go/bin  
    $ ls -lh ~/go/bin
    total 68M
    -rwxrwxr-x 1 commit commit 32M Aug  5 08:33 maincli
    -rwxrwxr-x 1 commit commit 37M Aug  5 08:33 maind
> .
  ```

# To verify the version


# Create a Wallet/Validator

```bash
maind init ${moniker}
```

> ```
$ ls -lh $HOME/.maind/config/
total 28K
-rw-r--r-- 1 commit commit  525 Aug  5 08:34 app.toml
-rw-r--r-- 1 commit commit 9.8K Aug  5 08:34 config.toml
-rw-r--r-- 1 commit commit 3.6K Aug  5 08:34 genesis.json
-rw------- 1 commit commit  148 Aug  5 08:34 node_key.json
-rw------- 1 commit commit  345 Aug  5 08:34 priv_validator_key.json
  ```

# Create a Genesis Transaction

# git clone $genesis
git clone https://github.com/commitHub/genesisTransactions
cd genesisTransactions
cp  $HOME/.maind/config/genesis.json .
export ip=$(curl ifconfig.me -s)
export node_id=$(maind tendermint show-node-id)
echo ${node_id}@${ip}:26656 > $HOME/.maind/config/peers.json
cp $HOME/.maind/config/peers.json .
# Verify by "cat $HOME/.maind/config/peers.json"

# Add gentx

# check you keys
# maincli keys show <your_wallet_name> -a
# e.g. 
#$ maincli keys show SWS -a
# commit1xhnkx9y9w58ylgtgytrxgzl2xsc3u8lglynsta

# Create an account with 1000000  tokens
# > kvd add-genesis-account $(kvcli keys show <your_wallet_name> -a) 1000000000000ukava
maind add-genesis-account $(maincli keys show SWS -a)
e.g.
```bash
maind add-genesis-account $(maincli keys show SWS -a) 1000000000000ucommit
```

Sign a gentx that creates your validator in the genesis file. Note to pass your public ip to the --ip flag. 
> kvd gentx --name <your_wallet_name> --amount 1000000000000ukava --ip <your-public-ip>
e.g.
```bash 
kvd gentx --name SWS  --amount 5000000000000ukava --ip  xxx.xxx.xxx.xxx

maind gentx --name SWS  --amount 1000000000000ucommit --ip 148.251.18.178

$ maind gentx --name SWS  --amount 1000000000000ucommit --ip 148.251.18.178
Password to sign with 'SWS':
Genesis transaction written to "/opt/validation/commit/.maind/config/gentx/gentx-348ad92411a134a49896e048fa9970135459195d.json"


```

This will write your genesis transaction to $HOME/.kvd/config/gentx/gentx-<gen-tx-hash>.json.

# Submit Genesis Transaction

To submit the gentx you created, fork the kava-testnets repo. Be sure you forked the repo at https://github.com/kava-labs/kava-testnets under your user name first. 

> ```
  $ cd $HOME 
  $ git clone git@github.com:<YOUR-USERNAME>/kava-testnets.git 
  $ cd kava-testnets 
  $ cp $HOME/.kvd/config/gentx/* $HOME/kava-testnets/2000/. 
  $ git push 

> .
  ```

e.g.

```bash
cd $HOME
git clone git@github.com:amitkarpe/kava-testnets.git
cd kava-testnets
cp $HOME/.kvd/config/gentx/* $HOME/kava-testnets/2000/
git push
```

Create a pull request for ```<github-username>/kava-testnets:master``` against the master branch of the Kava testnets repo.
